const API_KEY = "";
const FLOW_ID = "";
const FLOW_PATH = "";
const FLOWDOCK_S3_PATH = "https://fd-files-production.s3.amazonaws.com";
const REST_PATH = "https://flowdock.com/rest";

module.exports = { API_KEY, FLOW_ID, FLOW_PATH, FLOWDOCK_S3_PATH, REST_PATH };
