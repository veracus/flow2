import React, { Component } from "react";
import "./RoomMenu.css";

class RoomMenu extends Component {
  render() {
    const { users, name } = this.props;

    if (!users) return null;

    return (
      <div className="RoomMenu">
        <div className="RoomMenu-name">Room Name:{name}</div>
        Users: {users.length}
      </div>
    );
  }
}

export default RoomMenu;
