const { FLOW_PATH } = require("./constants");

function getFullImageLink(session, path) {
  return new Promise((resolve, reject) => {
    session.get(path, {}, (err, flow, response) => {
      if (err) {
        console.error("Could not get full URL", err);
        reject(err);
      }
      resolve(response.req.path);
    });
  });
}

function getFlowHistory(session, flow) {
  return new Promise((resolve, reject) => {
    session.get(FLOW_PATH, { limit: 5 }, async (err, flow, response) => {
      if (err) {
        console.error("Could not get flow history", err);
        reject(err);
      }

      // Getting full path for a file
      const results = response.body.map(async message => {
        if (message.event === "file") {
          return Object.assign(
            {},
            {
              ...message,
              content: {
                ...message.content,
                fullPath: await getFullImageLink(session, message.content.path)
              }
            }
          );
        }
        return message;
      });

      Promise.all(results).then(completed => {
        resolve(completed);
      });
    });
  });
}

function getPrivateChat(session) {
  return new Promise((resolve, reject) => {
    session.get("/private", {}, (err, flow, response) => {
      if (err) {
        console.error("Could not get private chat", err);
        reject(err);
      }
      resolve(response.body);
    });
  });
}

function sendFile(session, name, blob, type) {
  return new Promise(async (resolve, reject) => {
    session.post(
      FLOW_PATH,
      {
        event: "file",
        content: {
          data: blob,
          content_type: type,
          file_name: name
        }
      },
      (err, flow, response) => {
        if (err) {
          console.error("Could not write file", err);
          reject(err);
        }
        resolve(response.body);
      }
    );
  });
}

module.exports = { getFlowHistory, getPrivateChat, sendFile, getFullImageLink };
