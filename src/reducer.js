export default function reducer(state = {}, action) {
  switch (action.type) {
    case "connected":
      return Object.assign({}, { ...state, connected: action.data });
    case "INIT":
      return Object.assign(
        {},
        {
          ...state,
          flows: action.data.flows,
          messages: action.data.messages,
          activeFlow: action.data.activeFlow,
          privateChat: action.data.privateChat
        }
      );
    case "ADD_MESSAGE":
      return Object.assign(
        {},
        { ...state, messages: [...state.messages, action.data] }
      );
    case "EDIT_MESSAGE": {
      return Object.assign(
        {},
        {
          ...state,
          messages: state.messages.map(message => {
            if (message.id === action.data.content.message) {
              return Object.assign({}, message, {
                edited: action.data.sent,
                edited_at: action.data.created_at,
                content: action.data.content.updated_content
              });
            }
            return message;
          })
        }
      );
    }
    case "DELETE_MESSAGE":
      return Object.assign(
        {},
        {
          ...state,
          messages: state.messages.filter(message => {
            return message.id !== action.data.content.message;
          })
        }
      );
    default:
      return state;
  }
}
