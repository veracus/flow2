import React, { Component } from "react";
import { connect } from "react-redux";
import "./PrivateChat.css";

const mapStateToProps = state => {
  return {
    privateChat: state.privateChat
  };
};

class PrivateChat extends Component {
  renderAvatar(chat) {
    const user = chat.users.filter(user => user.nick === chat.name)[0];

    return (
      <div className="PrivateChat-icon">
        <img src={user.avatar} alt={chat.name} />
      </div>
    );
  }

  render() {
    const { privateChat } = this.props;

    if (!privateChat) return null;

    return (
      <div className="PrivateChat">
        {privateChat.map(chat => {
          if (chat.open) {
            return (
              <div className="PrivateChat-chat" key={chat.id}>
                {this.renderAvatar(chat)}
                <div className="PrivateChat-name">{chat.name}</div>
              </div>
            );
          }
          return null;
        })}
      </div>
    );
  }
}

export default connect(mapStateToProps)(PrivateChat);
