import React, { Component } from "react";
import "./ThreadIcon.css";

const generateHex = (threadId, threadCount) => {
  if (threadCount > 1) {
    return `#${Math.floor(
      parseFloat(`0.${threadId.match(/\d+/)[0]}`) * 16777215
    ).toString(16)}`;
  }
  return "#000000";
};

class ThreadIcon extends Component {
  render() {
    const { threadId, threadCount } = this.props;

    const iconStyle = {
      color: generateHex(threadId, threadCount)
    };

    return (
      <div className="ThreadIcon" style={iconStyle}>
        <i className="fas fa-angle-double-right" />
      </div>
    );
  }
}

export default ThreadIcon;
