import React, { Component } from "react";
import "./Users.css";

class Users extends Component {
  render() {
    const { users } = this.props;

    if (!users) return null;

    return (
      <div className="Users">
        {users.map(user => {
          return (
            <div className="Users-user" key={user.id}>
              <div className="Users-icon">
                <img src={user.avatar} alt={user.nick} />
              </div>
              <div className="Users-username">{user.nick}</div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Users;
