import React, { Component } from "react";
import { connect } from "react-redux";
import Flows from "./Flows";
import Room from "./Room";
import InputBox from "./InputBox";
import PrivateChat from "./PrivateChat";
import Users from "./Users";
import "./App.css";

const mapStateToProps = state => {
  return {
    flows: state.flows,
    activeFlow: state.activeFlow
  };
};

class App extends Component {
  constructor(props) {
    super(props);

    this.getUsersForRoom = this.getUsersForRoom.bind(this);
    this.getRoomName = this.getRoomName.bind(this);
  }

  getUsersForRoom() {
    return this.props.activeFlow ? this.props.activeFlow.users : null;
  }

  getRoomName() {
    return this.props.activeFlow ? this.props.activeFlow.name : null;
  }

  render() {
    return (
      <div className="App">
        <div className="App_container">
          <div className="App_flows">
            <Flows />
            <PrivateChat />
          </div>
          <div className="App_chat">
            <Room users={this.getUsersForRoom()} name={this.getRoomName()} />
            <InputBox />
          </div>
          <div className="App_users">
            <Users users={this.getUsersForRoom()} />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(App);
