const http = require("http");
const server = http.createServer();
const flowdock = require("flowdock");
const io = require("socket.io")();

const {
  getFlowHistory,
  getPrivateChat,
  sendFile,
  getFullImageLink
} = require("./FlowdockHelpers");
const { API_KEY, FLOW_ID } = require("./constants");

const session = new flowdock.Session(API_KEY);

io.on("connection", socket => {
  console.log("Socket connected: " + socket.id);

  socket.on("action", async action => {
    if (action.type === "server/LOAD") {
      session.flows(async (err, flows) => {
        const availableFlows = flows.map(flow => {
          return {
            id: flow.id,
            name: flow.name,
            description: flow.description,
            joined: flow.joined,
            open: flow.open,
            team_notifications: flow.team_notifications,
            users: flow.users,
            flowNameParam: flow.parameterized_name,
            organizationNameParam: flow.organization.parameterized_name
          };
        });

        // const activeFlow = flows[0];

        const activeFlow = availableFlows.filter(
          flow => flow.id === FLOW_ID
        )[0];

        const history = await getFlowHistory(session, activeFlow.id);
        const privateChat = await getPrivateChat(session);

        /*
        const stream = session.stream(
          availableFlows.map(flow => {
            return flow.id;
          })
        );
        */

        const stream = session.stream(activeFlow.id, { user: 1, active: true });

        stream.on("message", message => {
          handleMessage(message, socket);
        });

        socket.emit("action", {
          type: "INIT",
          data: {
            flows: availableFlows,
            messages: history,
            privateChat,
            activeFlow
          }
        });
      });

      console.log("Got hello data!", action.data);
      socket.emit("action", { type: "connected", data: true });
    }

    if (action.type === "server/SEND_MESSAGE") {
      const { flowId, message, tags } = action.data;

      session.message(flowId, message, tags);
    }

    if (action.type === "server/SEND_FILE") {
      const { name, blob, type } = action.data;

      await sendFile(session, name, blob, type);
    }

    if (action.type === "server/UPDATE_MESSAGE") {
      const { messageId, content, activeFlow } = action.data;
      const { flowNameParam, organizationNameParam } = activeFlow;

      session.editMessage(
        flowNameParam,
        organizationNameParam,
        messageId,
        { content: content },
        (err, message, response) => {
          if (err) {
            console.log(err);
          }
        }
      );
    }
  });
});

function handleMessage(message, socket) {
  const { event, content } = message;

  console.log(message);

  if (event === "file") {
    addFileToRoom(message, socket);
  }

  if (event === "activity.user" && content) {
    if (content.typing === false) {
      console.log(`User ${message.user} stopped typing`);
    }
    if (content.typing) {
      console.log(`User ${message.user} is typing`);
    }
  }

  if (event === "message" && message.thread && message.flow) {
    addMessageToRoom(message, socket);
  }

  if (event === "message-edit") {
    editMessage(message, socket);
  }

  if (event === "message-delete") {
    messageDeleted(message, socket);
  }
}

function messageDeleted(message, socket) {
  socket.emit("action", {
    type: "DELETE_MESSAGE",
    data: message
  });
}

function addMessageToRoom(message, socket) {
  socket.emit("action", {
    type: "ADD_MESSAGE",
    data: message
  });
}

function editMessage(message, socket) {
  socket.emit("action", {
    type: "EDIT_MESSAGE",
    data: message
  });
}

async function addFileToRoom(message, socket) {
  const fullPath = await getFullImageLink(session, message.content.path);
  socket.emit("action", {
    type: "ADD_MESSAGE",
    data: Object.assign(
      {},
      {
        ...message,
        content: {
          ...message.content,
          fullPath
        }
      }
    )
  });
}

io.attach(server);
server.listen(8000);
