import React, { Component } from "react";
import { connect } from "react-redux";
import "./Flows.css";

const mapStateToProps = state => {
  return {
    flows: state.flows,
    activeFlow: state.activeFlow
  };
};

class Flows extends Component {
  constructor(props) {
    super(props);

    this.generateFlowName = this.generateFlowName.bind(this);
  }

  generateFlowName(flow) {
    if (flow.id === this.props.activeFlow.id) {
      return <div className="Flows-flow-text">{`${flow.name} (active)`}</div>;
    }

    return <div className="Flows-flow-text">{flow.name}</div>;
  }

  generateFlowIcon(flow) {
    return (
      <div className="Flows-flow-icon">
        <div className="Flows-flow-icon-letter">
          {flow.name.charAt(0).toUpperCase()}
        </div>
      </div>
    );
  }

  render() {
    const { flows } = this.props;

    if (!flows) return null;

    return (
      <div className="Flows">
        {flows.map(flow => {
          return (
            <div className="Flows-flow" key={flow.id}>
              {this.generateFlowIcon(flow)}
              {this.generateFlowName(flow)}
            </div>
          );
        })}
      </div>
    );
  }
}

export default connect(mapStateToProps)(Flows);
