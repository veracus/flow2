import { createStore, applyMiddleware } from "redux";
import createSocketIoMiddleware from "redux-socket.io";
import reducer from "./reducer";
import io from "socket.io-client";

const socket = io("http://localhost:8000");
const socketIoMiddleware = createSocketIoMiddleware(socket, "server/");

const store = applyMiddleware(socketIoMiddleware)(createStore)(reducer);

store.subscribe(() => {
  console.log("new client state", store.getState());
});

export default store;
