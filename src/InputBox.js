import React, { Component } from "react";
import { connect } from "react-redux";
import { SEND_MESSAGE } from "./actions";
import UploadButton from "./UploadButton";
import "./InputBox.css";

const mapStateToProps = state => {
  return {
    activeFlow: state.activeFlow
  };
};

class InputBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };

    this.sendMessage = this.sendMessage.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }

  sendMessage(event) {
    event.preventDefault();

    const { activeFlow } = this.props;
    const { content } = this.state;
    const tags = [];

    this.setState(
      {
        content: ""
      },
      () => SEND_MESSAGE(activeFlow.id, content, tags)
    );
  }

  handleChange(event) {
    this.setState({
      content: event.target.value
    });
  }

  handleEnter(event) {
    event.keyCode === 13 ? this.sendMessage(event) : this.handleChange(event);
  }

  render() {
    const { content } = this.state;

    return (
      <div className="InputBox">
        <textarea
          className="InputBox-text"
          placeholder="Type a message.."
          maxLength={1000}
          value={content}
          onChange={this.handleChange}
          onKeyDown={this.handleEnter}
        />
        <UploadButton />
        <div className="InputBox-send-button" onClick={this.sendMessage}>
          <i className="fas fa-paper-plane" />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(InputBox);
