import React, { Component } from "react";
import { connect } from "react-redux";
import Dropzone from "react-dropzone";
import Message from "./Message";
import Loading from "./Loading";
import RoomMenu from "./RoomMenu";
import { LOAD_APP, SEND_FILE } from "./actions";
import "./Room.css";

const mapStateToProps = state => {
  return {
    messages: state.messages,
    file: {}
  };
};

class Room extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.clickButton = this.clickButton.bind(this);
    this.getSender = this.getSender.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  componentDidMount() {
    LOAD_APP();
  }

  getSender(userId) {
    const { users } = this.props;

    return users.filter(user => user.id.toString() === userId)[0];
  }

  clickButton() {
    LOAD_APP();
  }

  onDrop(files) {
    this.setState(
      {
        file: files[0]
      },
      () => {
        SEND_FILE(this.state.file);
      }
    );
  }

  render() {
    const { messages, users, name } = this.props;

    if (messages) {
      return (
        <Dropzone className="Room" onDrop={this.onDrop} disableClick={true}>
          <RoomMenu users={users} name={name} />
          {messages.map(message => {
            return (
              <Message
                key={message.id}
                payload={message}
                sender={this.getSender(message.user)}
              />
            );
          })}
        </Dropzone>
      );
    }

    return (
      <div className="Room">
        <Loading />
      </div>
    );
  }
}

export default connect(mapStateToProps)(Room);
