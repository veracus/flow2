import store from "./sockets";

export function LOAD_APP() {
  store.dispatch({ type: "server/LOAD", data: "LOAD" });
}

export function SEND_MESSAGE(flowId, message, tags) {
  store.dispatch({
    type: "server/SEND_MESSAGE",
    data: {
      flowId,
      message,
      tags
    }
  });
}

export function UPDATE_MESSAGE(messageId, content, activeFlow) {
  store.dispatch({
    type: "server/UPDATE_MESSAGE",
    data: {
      messageId,
      content,
      activeFlow
    }
  });
}

export function SEND_FILE(file) {
  const reader = new FileReader();
  const { name, type } = file;

  reader.onload = event => {
    store.dispatch({
      type: "server/SEND_FILE",
      data: {
        name,
        blob: event.target.result.split(",")[1],
        type
      }
    });
  };

  reader.readAsDataURL(file);
}
