import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import marked from "marked";
import ThreadIcon from "./ThreadIcon";
import { FLOWDOCK_S3_PATH } from "./constants";
import { UPDATE_MESSAGE } from "./actions";
import "./Message.css";

const mapStateToProps = state => {
  return {
    activeFlow: state.activeFlow
  };
};

class Message extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showEditButton: false,
      editView: false,
      content: this.props.payload.content
    };

    this.rawMarkup = this.rawMarkup.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.onMessageHover = this.onMessageHover.bind(this);
    this.onMessageExit = this.onMessageExit.bind(this);
    this.renderEditButton = this.renderEditButton.bind(this);
    this.renderMessageView = this.renderMessageView.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
    this.renderEditView = this.renderEditView.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  rawMarkup(content) {
    return { __html: marked(content, { sanitize: true }) };
  }

  renderMessage(content) {
    if (typeof content === "string") {
      return <span dangerouslySetInnerHTML={this.rawMarkup(content)} />;
    }

    return (
      <div>
        <img
          src={`${FLOWDOCK_S3_PATH}${content.fullPath}`}
          alt={content.file_name}
          height={content.image.height}
          width={content.image.width}
        />
      </div>
    );
  }

  renderEdited(edited) {
    return edited ? ` (Edited ${moment(edited).format("L")}) ` : null;
  }

  onMessageHover() {
    this.setState({
      showEditButton: true
    });
  }

  onMessageExit() {
    this.setState({
      showEditButton: false
    });
  }

  toggleEdit() {
    this.setState({
      editView: !this.state.editView
    });
  }

  renderEditButton() {
    const { showEditButton } = this.state;

    if (showEditButton) {
      return <button onClick={this.toggleEdit}>Edit</button>;
    }

    return null;
  }

  renderMessageView() {
    const { sender, payload } = this.props;
    const { content, created_at, edited_at, thread } = payload;

    return (
      <div
        className="Message"
        onMouseEnter={this.onMessageHover}
        onMouseLeave={this.onMessageExit}
      >
        <ThreadIcon
          threadId={thread.id}
          threadCount={thread.internal_comments}
        />
        <img src={sender.avatar} alt={sender.nick} />
        <div className="Message-user">{`${sender.nick}  (${moment(
          created_at
        ).calendar()})`}</div>
        <div className="Message-message">
          {this.renderMessage(content)}
          {this.renderEdited(edited_at)}
          {this.renderEditButton()}
        </div>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();

    const { payload, activeFlow } = this.props;
    const { content } = this.state;

    UPDATE_MESSAGE(payload.id, content, activeFlow);

    this.toggleEdit();
  }

  handleChange(event) {
    this.setState({
      content: event.target.value
    });
  }

  renderEditView() {
    const { sender, payload } = this.props;
    const { created_at, thread } = payload;
    const { content } = this.state;

    return (
      <div className="Message">
        <ThreadIcon
          threadId={thread.id}
          threadCount={thread.internal_comments}
        />
        <img src={sender.avatar} alt={sender.nick} />
        <div className="Message-user">{`${sender.nick}  (${moment(
          created_at
        ).calendar()})`}</div>
        <div className="Message-message">
          <form onSubmit={this.handleSubmit}>
            <input type="text" value={content} onChange={this.handleChange} />
            <input type="submit" value="Send" />
          </form>
        </div>
      </div>
    );
  }

  render() {
    const { payload } = this.props;
    const { editView } = this.state;

    if (payload) {
      if (editView) {
        return this.renderEditView();
      }
      return this.renderMessageView();
    }
    return null;
  }
}

export default connect(mapStateToProps)(Message);
