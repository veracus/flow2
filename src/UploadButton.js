import React, { Component } from "react";
import Dropzone from "react-dropzone";
import { SEND_FILE } from "./actions";
import "./UploadButton.css";

class UploadButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: {}
    };

    this.handleDrop = this.handleDrop.bind(this);
  }

  handleDrop(files) {
    this.setState(
      {
        file: files[0]
      },
      () => {
        SEND_FILE(this.state.file);
      }
    );
  }

  render() {
    return (
      <Dropzone className="UploadButton" onDrop={this.handleDrop}>
        <div className="UploadButton-paperclip">
          <i className="fas fa-paperclip" />
        </div>
      </Dropzone>
    );
  }
}

export default UploadButton;
